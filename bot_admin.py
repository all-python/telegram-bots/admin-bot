import asyncio
from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ChatMemberAdministrator
from aiogram.dispatcher.filters import Command

API_TOKEN = 'здесь должен быть токен'
bot = Bot(token=API_TOKEN, parse_mode="HTML")
dp = Dispatcher(bot)

@dp.message_handler(commands=["name"])
async def name(message: types.Message, command):
    if command.args:
        await bot.promote_chat_member(message.chat.id, message.from_user.id, can_pin_messages=True)
        await bot.set_chat_administrator_custom_title(message.chat.id, message.from_user.id, command.args)
        await message.answer(f"Привет, <b>{command.args}</b>")
    else:
        await message.answer("Пожалуйста, укажи своё имя после команды /name!")

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)

